require 'yaml'

# Select configuration file
CONFIG_FILE = ENV["KUBE_CONFIG"] || 'config.yml'

settings = YAML.load_file CONFIG_FILE

# Load configuration parameters
NETWORK_PREFIX = settings['network_prefix']
BRIDGE         = settings['bridge']
LB_RAM         = settings['lb_ram']
LB_CPUS        = settings['lb_cpus']
MASTER_RAM     = settings['master_ram']
MASTER_CPUS    = settings['master_cpus']
MASTER_NUM     = settings['master_num']
WORKER_RAM     = settings['worker_ram']
WORKER_CPUS    = settings['worker_cpus']
START_NODE     = settings['start_worker']
NUM_NODES      = settings['num_workers']
DEPLOY_MASTER  = settings['master']
DEPLOY_LB      = settings['lb']
BOX            = settings['vm_box']

Vagrant.configure("2") do |config|
  # Load Balancer
  if DEPLOY_LB
    config.vm.define :lb do |lb|
      lb.vm.box = BOX

      lb.vm.hostname = "lb"

      lb.vm.network :public_network,
      :bridge => "#{BRIDGE}"

      lb.vm.provider :virtualbox do |vb|
        vb.name = "lb"
        vb.check_guest_additions = false
        vb.memory = LB_RAM
        vb.cpus = LB_CPUS
      end
    end
  end

  # Master
  if DEPLOY_MASTER
    config.vm.define "master#{MASTER_NUM}" do |master|
      master.vm.box = BOX

      master.vm.hostname = "master#{MASTER_NUM}"

      master.vm.network :public_network,
      :bridge => "#{BRIDGE}"

      master.vm.provider :virtualbox do |vb|
        vb.name = "master#{MASTER_NUM}"
        vb.check_guest_additions = false
        vb.memory = MASTER_RAM
        vb.cpus = MASTER_CPUS
      end
    end
  end

  # Workers
  (START_NODE..START_NODE+NUM_NODES-1).each do |i|
    config.vm.define "node#{i}" do |node|
      node.vm.box = BOX
      node.vm.hostname = "node#{i}"

      node.vm.network :public_network,
      :bridge => "#{BRIDGE}"

      node.vm.provider :virtualbox do |vb|
        vb.name = "node#{i}"
        vb.check_guest_additions = false
        vb.memory = WORKER_RAM
        vb.cpus = WORKER_CPUS
      end
    end
  end

  # Create shared folder
  config.vm.synced_folder ".", "/vagrant", :mount_options => ['dmode=774', 'fmode=775']

  # Generate Ansible inventory
  config.vm.provision :ansible do |ansible|
    ansible.playbook = ".dummy.yml"

    ansible.groups = {
      "lbs"     => ["lb"],
      "masters" => ["master#{MASTER_NUM}"],
      "workers"   => ["node[#{START_NODE}:#{START_NODE+NUM_NODES-1}]"]
    }
  end
end
