# Kubernetes Cluster

## Description

This project aims at deploying a Kubernetes cluster using the VirtualBox hypervisor, Vagrant for easy creation of the virtual machines and Ansible for easy provisioning. Currently, this works by bridging the network interface of the host machine.

### File Structure

```
ROOT
  ├── hosts         -- Folder to store the Ansible inventories of multiple machines
  ├── roles         -- Folder containing the Ansible roles
  ├── config.yml    -- Default configuration file
  ├── provision.yml -- Ansible playbook to provision everything
  ├── README.md     -- This file
  └── Vagrantfile   -- Vagrant instructions
```

## Configuration

For configuring the deployment, the only file that needs to be touched is `config.yml` and has instructions on what is the expected configuration.

The configuration file path can be changed by changing the environment variable `KUBE_CONFIG` to the desired path (which allows for multiple configuration files to coexist independently).


## Deployment

To create the virtual machines, the only command we need to run is `vagrant up`.

Before provisioning the virtual machines, ensure that you have a public key with name `id_ed25519.pub` in your `~/.ssh` folder, as well as the respective private key, in order to facilitate SSH access to the Kubernetes cluster.

**WARNING:** When running with multiple physical machines and the `.vagrant` folder is shared, it is advised to copy the Ansible inventory before deploying virtual machines on a new physical machine. So, we have the `hosts` folder that will store the inventories of each machine. In order to copy the inventory, we should run the following command

```Shell
cp .vagrant/provisioners/ansible/inventory/vagrant_ansible_inventory hosts/<name.cfg>
```

---

To provision the virtual machines, we need to use the following command:

```Shell
ansible-playbook --ssh-extra-args '-o StrictHostKeyChecking=no' -i hosts/<name.cfg> all.yml
```

After this, we can get inside the machine with `ssh kube@127.0.0.1 -p 2222` and manage our cluster.

**NOTE:** Any file or folder that is present in the root directory of this project, *i.e.* that is on this folder, is going to be mounted on all virtual machines in the `/vagrant` directory, allowing us to easily deploy any project.
